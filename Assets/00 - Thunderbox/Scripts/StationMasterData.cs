﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StationMaster;

public class StationMasterData : MonoBehaviour
{

    public string[] defaultNames = new string[12] { "Case Player", "Top Hat Player", "Perfume Player", "Boot Player", "Book Player", "Teacup Player", "Camera Player", "Derby Player", "Flask Player", "Purse Player", "Ticket Player", "Teapot Player"}; //Must be in the same order as the playerPiece enum.
    public string[] customtNames = new string[12] { "Case Player", "Top Hat Player", "Perfume Player", "Boot Player", "Book Player", "Teacup Player", "Camera Player", "Derby Player", "Flask Player", "Purse Player", "Ticket Player", "Teapot Player"}; //Must be in the same order as the playerPiece enum.
    public List<locomotive> locomotiveStart = new List<locomotive>()
    {
        locomotive.loco_3,
        locomotive.loco_3_exec,
        locomotive.loco_4,
        locomotive.loco_4_exec,
        locomotive.loco_5_a,
        locomotive.loco_5_a,
        locomotive.loco_5_b,
        locomotive.loco_5_b,
        locomotive.loco_5_freight,
        locomotive.loco_6,
        locomotive.loco_6,
        locomotive.loco_7,
        locomotive.loco_8
    };
    public List<train> depot_trains = new List<train>();
    public int[] locoLengths = new int[11] //Must be in the same order as the locomotives enum.
    {
        3,
        3,
        4,
        4,
        5,
        5,
        5,
        6,
        7,
        8,
        0
    };
    public locoEffect[] locoEffects = new locoEffect[10]
{
        locoEffect.none,
        locoEffect.exec,
        locoEffect.none,
        locoEffect.exec,
        locoEffect.none,
        locoEffect.none,
        locoEffect.freight,
        locoEffect.none,
        locoEffect.none,
        locoEffect.none
};
    public List<locomotive> locomotiveDeck = new List<locomotive>();
    public List<player> players = new List<player>();
    public train currentTrain;
    public int[] carScores = new int[17] //Must be in the same order as the car enum.
    {
        1,
        2,
        3,
        4,
        5,
        6,
        -1,
        -2,
        -3,
        -4,
        -5,
        -6,
        0,
        0,
        0,
        0,
        0
    };
    public Sprite[] locomotiveSprites;
    public List<trainScore> trainScores = new List<trainScore>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void resetLocomotiveDeck()
    {
        locomotiveDeck.RemoveRange(0, locomotiveDeck.Count);
        int i = 0;
        while (i<locomotiveStart.Count)
        {
            locomotiveDeck.Add(locomotiveStart[i]);
            i++;
        }
    }

    public void clearDepot()
    {
        depot_trains.RemoveRange(0, depot_trains.Count);
    }

    public void clearPlayers()
    {
        players.RemoveRange(0,players.Count);
    }

    public void addPlayer(playerPiece piece)
    {
        players.Add(new player(piece, customtNames[(int)piece]));               
    }

    public void combineScores(bool sortScores = true)
    {
        int i = 0;
        while (i<players.Count)
        {
            int j = 0;
            while (j < trainScores.Count)
            {
                if (trainScores[j].piece == players[i].piece)
                {
                    players[i].score = players[i].score + trainScores[j].score;
                    if (players[i].score < 0) players[i].score = 0;
                    if (trainScores[j].score > players[i].topTrain) players[i].topTrain = trainScores[j].score;
                }

                j++;
            }
            i++;
        }
        if (sortScores) players.Sort((playerA, playerB) => playerB.score.CompareTo(playerA.score));
    }

}
