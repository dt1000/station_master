﻿using UnityEngine;
using UnityEngine.UI;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class setUpFinalScores : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(GameObject))]
        public FsmArray UIObjects;

        [ActionSection("Output")]

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(GameObject))]
        public FsmArray ShowObjects;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            ShowObjects = null;
            UIObjects = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();                        

            int i = 0;
            //Deactivate all scores and set them in the middle.
            while ( i < UIObjects.Values.Length)
            {
                ((GameObject)UIObjects.Values[i]).SetActive(false);
                ((GameObject)UIObjects.Values[i]).GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
                i++;
            }

            ShowObjects.Resize(theData.players.Count);
            i = 0;
            while (i<theData.players.Count)
            {
                GameObject theUI = (GameObject)UIObjects.Values[(int)theData.players[i].piece];
                ShowObjects.Values[i] = theUI;
                theUI.transform.Find("Ticket/Player Name").GetComponent<TMPro.TextMeshProUGUI>().text = theData.players[i].name;
                theUI.transform.Find("Ticket/Score").GetComponent<TMPro.TextMeshProUGUI>().text = theData.players[i].score.ToString();
                int rando = Random.Range(0, 99999999);
                theUI.transform.Find("Ticket/Ticket No. Left").GetComponent<TMPro.TextMeshProUGUI>().text = rando.ToString("00000000");
                theUI.transform.Find("Ticket/Ticket No. Right").GetComponent<TMPro.TextMeshProUGUI>().text = rando.ToString("00000000");                
                i++;
            }

            Finish ();
		}
	}
}
