﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class showTrainScores : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(Vector2))]
        public FsmArray placement;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(GameObject))]
        public FsmArray UIObjects;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            placement = null;
            UIObjects = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();                        

            int i = 0;
            //Deactivate all scores
            while ( i < UIObjects.Values.Length)
            {
                ((GameObject)UIObjects.Values[i]).SetActive(false);
                i++;
            }

            i = 0;
            while (i<theData.players.Count)
            {
                GameObject theUI = (GameObject)UIObjects.Values[(int)theData.trainScores[i].piece];
                theUI.transform.Find("Player Name").GetComponent<TMPro.TextMeshProUGUI>().text = theData.trainScores[i].playerName;
                theUI.transform.Find("Score").GetComponent<TMPro.TextMeshProUGUI>().text = theData.trainScores[i].score.ToString();
                theUI.GetComponent<RectTransform>().anchoredPosition = (Vector2)placement.Values[i];
                int rando = Random.Range(0, 99999999);
                theUI.transform.Find("Ticket No. Left").GetComponent<TMPro.TextMeshProUGUI>().text = rando.ToString("00000000");
                theUI.transform.Find("Ticket No. Right").GetComponent<TMPro.TextMeshProUGUI>().text = rando.ToString("00000000");
                theUI.SetActive(true);
                i++;
            }

            Finish ();
		}
	}
}
