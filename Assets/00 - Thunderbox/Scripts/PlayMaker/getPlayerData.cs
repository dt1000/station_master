﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class getPlayerData : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        public FsmInt index;

        [ActionSection("Output")]

        [UIHint(UIHint.Variable)]
        public FsmString playerName;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(playerPiece))]
        public FsmEnum playerPiece;

        [UIHint(UIHint.Variable)]
        public FsmInt playerScore;

        [UIHint(UIHint.Variable)]
        public FsmInt playerTopTrain;

        [ActionSection("Events")]

        public FsmEvent gotData;
        public FsmEvent noData;



        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            index = 0;
            playerName = null;
            playerPiece = null;
            playerScore = null;
            playerTopTrain = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            if (theData.players.Count > index.Value)
            {
                playerName.Value = theData.players[index.Value].name;
                playerPiece.Value = theData.players[index.Value].piece;
                playerScore.Value = theData.players[index.Value].score;
                playerTopTrain.Value = theData.players[index.Value].topTrain;
                Fsm.Event(gotData);
            }
            else
            {
                Fsm.Event(noData);
            }

            Finish ();
		}
	}
}
