﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Thunderbox")]
    public class getScreenDiagonal : FsmStateAction
    {
        [ActionSection("Output")]
        public FsmFloat diagonalInches;

        [ActionSection("Events")]
        public FsmEvent phone;
        public FsmEvent tablet;

        public override void Reset()
        {
            diagonalInches = null;
            phone = null;
            tablet = null;
        }

        public override void OnEnter()
        {
           
            float screenWidth = Screen.width / Screen.dpi;
            float screenHeight = Screen.height / Screen.dpi;
            float size = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));
            diagonalInches.Value = size;

            bool isTablet = (size >= 6.6f);

            if (isTablet)
            {
                Fsm.Event(tablet);
            }
            else
            {
                Fsm.Event(phone);
            }

            Finish();
        }
    }
}
