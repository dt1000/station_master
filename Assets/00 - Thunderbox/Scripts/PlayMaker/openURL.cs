﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Thunderbox")]
    [Tooltip("Open a URL.")]
    public class openURL : FsmStateAction
    {
        [ActionSection("Data")]

        [Tooltip("The URL to Open.")]
        public FsmString URL;

        public override void Reset()
        {
            URL = "http://thunderboxentertainment.com";
        }

        public override void OnEnter()
        {

            Application.OpenURL(URL.Value);

            Finish();
        }
    }
}
