﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class getLocoData : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locomotive))]
        public FsmEnum loco;

        [ActionSection("Output")]

        [UIHint(UIHint.Variable)]
        public FsmInt score;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locoEffect))]
        public FsmEnum effect;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            loco = null;
            score = null;
            effect = locoEffect.none;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            score.Value = theData.locoLengths[(int)(locomotive)loco.Value];
            effect.Value = theData.locoEffects[(int)(locomotive)loco.Value];

            Finish ();
		}
	}
}
