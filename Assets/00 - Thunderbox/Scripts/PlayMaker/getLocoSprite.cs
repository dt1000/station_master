﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class getLocoSprite : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locomotive))]
        public FsmEnum loco;

        [ActionSection("Output")]

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(Sprite))]
        public FsmObject locoSprite;


        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            loco = null;
            locoSprite = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            locoSprite.Value = theData.locomotiveSprites[(int)(locomotive)loco.Value];

            Finish ();
		}
	}
}
