﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class initialisePlayers : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        public FsmArray selectedPieces;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            selectedPieces = null;
        }
		
		public override void OnEnter()
		{
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            theData.clearPlayers();

            int i = 0;

            while (i < selectedPieces.Values.Length)
            {
                theData.addPlayer((StationMaster.playerPiece)selectedPieces.Values[i]);

                i++;
            }

			Finish ();
		}
	}
}
