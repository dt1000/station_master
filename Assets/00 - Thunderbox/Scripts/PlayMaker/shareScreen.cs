﻿using UnityEngine;
using System.IO;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class shareScreen : FsmStateAction
	{
        
        public FsmString title;
        public FsmString message;




        public override void Reset()
		{
            title = null;
            message = null;
        }
		
		public override void OnEnter()
		{
            Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            ss.Apply();

            string filePath = Path.Combine(Application.temporaryCachePath, "stationMaster.png");
            File.WriteAllBytes(filePath, ss.EncodeToPNG());

            // To avoid memory leaks
            Object.Destroy(ss);

            new NativeShare().AddFile(filePath).SetSubject(title.Value).SetText(message.Value).Share();

            // Share on WhatsApp only, if installed (Android only)
            //if( NativeShare.TargetExists( "com.whatsapp" ) )
            //	new NativeShare().AddFile( filePath ).SetText( "Hello world!" ).SetTarget( "com.whatsapp" ).Share();

            Finish();
		}
	}
}
