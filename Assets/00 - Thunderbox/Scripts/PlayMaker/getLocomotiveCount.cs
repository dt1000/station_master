﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class getLocomotiveCount : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locomotive))]
        public FsmEnum loco;

        [ActionSection("Output")]

        public FsmInt locoCount;
        public FsmEvent no;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            loco = null;
            locoCount = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            locoCount.Value = 0;
            int i = 0;

            while ( i < theData.locomotiveDeck.Count)
            {
                if (theData.locomotiveDeck[i] == (locomotive)loco.Value) locoCount.Value++;
                i++;
            }

            Finish();
        }
	}
}
