﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	[Tooltip("Link to the App Store for rating.")]
	public class openAppStore : FsmStateAction
	{
		
		[ActionSection("Android")]
		
		[UIHint(UIHint.FsmString)]
		[Tooltip("Your bundle identifyer from the Player Settings.")]
		public FsmString bundleID;

		[ActionSection("iOS")]
		
		[UIHint(UIHint.FsmString)]
		[Tooltip("Your app's slug from iTunes connect.")]
		public FsmString slug;

		[UIHint(UIHint.FsmString)]
		[Tooltip("Your app ID from iTunes connect.")]
		public FsmString appID;

		public override void Reset()
		{
			bundleID = null;
			slug = null;
			appID = null;
		}
		
		public override void OnEnter()
		{
			#if UNITY_ANDROID
			string theURL = "market://details?id=" + bundleID.Value;
			Application.OpenURL(theURL);
			#elif UNITY_IPHONE
			string theURL = "itms-apps://itunes.apple.com/app/" + slug.Value + "/id" + appID.Value;
			Application.OpenURL(theURL);
			#endif
			Finish ();
		}
	}
}
