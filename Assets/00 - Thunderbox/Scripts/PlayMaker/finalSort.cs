﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Thunderbox")]
    public class finalSort : FsmStateAction
    {
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        public bool reverse;

        public override void Reset()
        {
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            reverse = false;
        }

        public override void OnEnter()
        {
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            //theData.players.Sort((playerA, playerB) => playerB.score.CompareTo(playerA.score));

            if (!reverse)
            {
                theData.players.Sort((playerA, playerB) => playerA.score == playerB.score ? playerA.topTrain.CompareTo(playerB.topTrain) : playerA.score.CompareTo(playerB.score));
            }
            else
            {
                theData.players.Sort((playerA, playerB) => playerB.score == playerA.score ? playerB.topTrain.CompareTo(playerA.topTrain) : playerB.score.CompareTo(playerA.score));
            }
            

            Finish();
        }
    }
}
