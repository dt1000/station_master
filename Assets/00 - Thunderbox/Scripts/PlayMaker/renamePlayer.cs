﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class renamePlayer : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        public FsmString playerName;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(playerPiece))]
        public FsmEnum playerPiece;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            playerName = null;
            playerPiece = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            int i = 0;

            while (i < theData.players.Count)
            {
                if (theData.players[i].piece == (playerPiece)playerPiece.Value) theData.players[i].name = playerName.Value;
                i++;
            }            

            Finish ();
		}
	}
}
