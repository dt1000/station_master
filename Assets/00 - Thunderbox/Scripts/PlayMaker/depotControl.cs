﻿using UnityEngine;
using System.Collections.Generic;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class addTrainToDepot : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locomotive))]
        public FsmEnum locomotiveType;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(car))]
        public FsmArray carriages;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            locomotiveType = null;
            carriages = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            List<car> carList = new List<car>();

            int i = 0;

            while(i<carriages.Length)
            {
                carList.Add((car)carriages.Values[i]);
                i++;
            }

            theData.depot_trains.Add(new train((locomotive)locomotiveType.Value, carList));

            Finish ();
		}
	}

    [ActionCategory("Thunderbox")]
    public class removeTrainFromDepot : FsmStateAction
    {
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        public FsmInt index;


        public override void Reset()
        {
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            index = 0;
        }

        public override void OnEnter()
        {
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            theData.depot_trains.RemoveAt(index.Value);

            Finish();
        }
    }

    [ActionCategory("Thunderbox")]
    public class clearDepot : FsmStateAction
    {
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        public override void Reset()
        {
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
        }

        public override void OnEnter()
        {
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            theData.depot_trains.RemoveRange(0, theData.depot_trains.Count);

            Finish();
        }
    }

    [ActionCategory("Thunderbox")]
    public class checkDepotLoco : FsmStateAction
    {
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locomotive))]
        public FsmEnum locomotiveType;

        [ActionSection("Output")]
        public FsmInt locoCount;

        public override void Reset()
        {
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            locomotiveType = null;
            locoCount = null;
        }

        public override void OnEnter()
        {
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            locoCount.Value = 0;
            int i = 0;

            while (i < theData.depot_trains.Count)
            {
                if (theData.depot_trains[i].locoType == (locomotive)locomotiveType.Value) locoCount.Value++;
                i++;
            }
            Finish();
        }
    }

    [ActionCategory("Thunderbox")]
    public class getDepotTrain : FsmStateAction
    {
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locomotive))]
        public FsmEnum locomotiveType;

        [ActionSection("Output")]

        [UIHint(UIHint.Variable)]
        public FsmInt Index;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(car))]
        public FsmArray carriages;

        public override void Reset()
        {
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            locomotiveType = null;
            Index = null;
            carriages = null;
        }

        public override void OnEnter()
        {
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            carriages.Resize(0);

            int i = 0;

            while (i < theData.depot_trains.Count)
            {              
                if (theData.depot_trains[i].locoType == (locomotive)locomotiveType.Value)
                {
                    Index.Value = i;
                    int j = 0;
                    while (j< theData.depot_trains[i].cars.Count)
                    {
                        carriages.Resize(carriages.Length + 1);
                        carriages.Set(carriages.Length - 1, theData.depot_trains[i].cars[j]);
                        j++;
                    }
                }                
                i++;
            }
            Finish();
        }
    }

    [ActionCategory("Thunderbox")]
    public class getDepotTrains : FsmStateAction
    {
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locomotive))]
        public FsmEnum locomotiveType;

        [ActionSection("Output")]

        [UIHint(UIHint.Variable)]
        public FsmInt indexA;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(car))]
        public FsmArray carriagesA;

        [UIHint(UIHint.Variable)]
        public FsmInt indexB;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(car))]
        public FsmArray carriagesB;

        public override void Reset()
        {
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            locomotiveType = null;
            indexA = null;
            indexB = null;
            carriagesA = null;
            carriagesB = null;
        }

        public override void OnEnter()
        {
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            carriagesA.Resize(0);
            carriagesB.Resize(0);

            bool doneA = false;

            int i = 0;

            while (i < theData.depot_trains.Count)
            {
                if (theData.depot_trains[i].locoType == (locomotive)locomotiveType.Value)
                {
                    if (!doneA)
                    {
                        int j = 0;
                        indexA.Value = i;
                        while (j < theData.depot_trains[i].cars.Count)
                        {
                            carriagesA.Resize(carriagesA.Length + 1);
                            carriagesA.Set(carriagesA.Length - 1, theData.depot_trains[i].cars[j]);
                            j++;
                        }
                        doneA = true;
                    }
                    else
                    {
                        int j = 0;
                        indexB.Value = i;
                        while (j < theData.depot_trains[i].cars.Count)
                        {
                            carriagesB.Resize(carriagesB.Length + 1);
                            carriagesB.Set(carriagesB.Length - 1, theData.depot_trains[i].cars[j]);
                            j++;
                        }
                    }
                    
                }
                i++;
            }
            Finish();
        }
    }

    public class beforeDepot : FsmStateAction
    {
        [ActionSection("Input")]

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(car))]
        public FsmArray carraiges;

        [UIHint(UIHint.Variable)]
        public FsmInt index;

        [ActionSection("output")]

        [UIHint(UIHint.Variable)]
        public FsmBool before;

        public override void Reset()
        {
            carraiges = null;
            index = null;
            before = null;
        }

        public override void OnEnter()
        {
            int depotIndex = 0;
            int i = 0;
            while(i< carraiges.Values.Length)
            {
                if ((car)carraiges.Values[i] == car.depot) depotIndex = i;
                i++;
            }

            before.Value = (index.Value < depotIndex);

            Finish();

        }
    }

    [ActionCategory("Thunderbox")]
    public class depotTransfer : FsmStateAction
    {
        [ActionSection("Input")]


        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(car))]
        public FsmArray sourceArray;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(car))]
        public FsmArray targetArray;

        public override void Reset()
        {
            sourceArray = null;
            targetArray = null;
        }

        public override void OnEnter()
        {

            targetArray.Resize(sourceArray.Values.Length);

            int i = 0;

            while (i < sourceArray.Values.Length)
            {
                
                targetArray.Set(i, (car)sourceArray.Values[i]);
                i++;
            }
            Finish();
        }
    }
}
