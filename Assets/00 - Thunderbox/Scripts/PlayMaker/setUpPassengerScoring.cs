﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class setUpPassengerScoring : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(GameObject))]
        public FsmArray UIObjects;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(float))]
        public FsmArray startOffsets;

        [UIHint(UIHint.Variable)]
        public FsmFloat spacing;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(int))]
        public FsmArray playerScores;

        [ActionSection("Output")]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(GameObject))]
        public FsmArray playerUIObjects;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            UIObjects = null;
            startOffsets = null;
            spacing = null;
            playerScores = null;
            playerUIObjects = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            int playerCount = theData.players.Count;
            float startOffset = (float)startOffsets.Values[playerCount - 1];
            int i = 0;
            while ( i < UIObjects.Length)
            {
                ((GameObject)UIObjects.Values[i]).SetActive(false);
                i++;
            }

            i = 0;
            while (i < playerCount)
            {
                GameObject thePiece = (GameObject)UIObjects.Values[(int)theData.players[i].piece];
                RectTransform theRect = thePiece.GetComponent<RectTransform>();
                //theRect.anchoredPosition = new Vector2((startOffset + (i * 150f)), theRect.anchoredPosition.y);
                theRect.anchoredPosition = new Vector2((startOffset + (i * spacing.Value)), theRect.anchoredPosition.y);

                GameObject scroller = thePiece.transform.Find("Scroller Mask/Scroller").gameObject;

                playerUIObjects.Resize(playerUIObjects.Length + 1);
                playerUIObjects.Values[playerUIObjects.Length - 1] = scroller;
              
                thePiece.SetActive(true);
                i++;
            }
            Finish ();
		}
	}
}
