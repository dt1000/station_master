﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class upateCustomNames : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(string))]
        public FsmArray nameArray;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            nameArray = FsmVariables.GlobalVariables.GetFsmArray("Custom Names");
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            int i = 0;

            while (i < theData.players.Count)
            {
                theData.customtNames[(int)theData.players[i].piece] = theData.players[i].name;
                i++;
            }

            i = 0;

            while (i<12)
            {
                nameArray.Values[i] = theData.customtNames[i];
                i++;
            }

            Finish ();
		}
	}
}
