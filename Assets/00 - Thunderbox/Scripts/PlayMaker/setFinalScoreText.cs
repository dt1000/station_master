﻿using UnityEngine;
using UnityEngine.UI;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Thunderbox")]
    public class setFinalScoreText : FsmStateAction
    {
        private string[] suffixes = new string[7] { "th", "st", "nd", "rd", "th", "th", "th" };

        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        public FsmInt index;

        [UIHint(UIHint.Variable)]
        public FsmGameObject hintObject;

        public override void Reset()
        {
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            index = null;
            hintObject = null;
        }

        public override void OnEnter()
        {
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            int position = theData.players.Count - index.Value;

            if (position > 1)
            {
                hintObject.Value.GetComponent<TMPro.TextMeshProUGUI>().text = theData.players[index.Value].name + " is in " + position.ToString("0") + suffixes[position] + " place with " + theData.players[index.Value].score.ToString() + " points.";
            }
            else
            {
                hintObject.Value.GetComponent<TMPro.TextMeshProUGUI>().text = theData.players[index.Value].name + " wins with " + theData.players[index.Value].score.ToString() + " points!";
            }

            Finish();
        }
    }
}
