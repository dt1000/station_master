﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Thunderbox")]
    public class calculateCarriageScores : FsmStateAction
    {
        [ActionSection("Input")]

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locoEffect))]
        public FsmEnum effect;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(car))]
        public FsmArray carriages;

        [UIHint(UIHint.Variable)]
        public FsmBool fromDepot;

        [ActionSection("Output")]

        [UIHint(UIHint.Variable)]
        public FsmInt trainScore;

        private int[] normalScores = new int[17] {1,2,3,4,5,6,-1,-2,-3,-4,-5,-6,-3,-4,0,0,0};
        private int[] execScores = new int[17] { 1, 2, 3, 4, 5, 6, -1, -2, -3, -4, -5, -6, 6, 7, 0, 0, 0 };
        private int[] freightScores = new int[17] { -1, -2, -3, -4, -5, -6, 1, 2, 3, 4, 5, 6, -3, -4, 0, 0, 0 };           

    public override void Reset()
		{
            effect = null;
            carriages = null;
            //scoreFromDepot = null;
            trainScore = null;
        }
		
		public override void OnEnter()
		{
            int i = 0;
            int royalCars = 2;
            bool gotRoyal = false;
            bool gotDepot = false;
            bool scoreFromDepot = fromDepot.Value;
            int royalIndex = 0;
            int depotIndex = 0;
            int score = 0;
            int startIndex = 0;

            //Check for Royal Car
            while (i < carriages.Length)
            {
                if ((car)carriages.Values[i] == car.royal)
                {
                    gotRoyal = true;
                    royalIndex = i;
                }
                i++;
            }

            //Check for Depot
            i = 0;
            while (i < carriages.Length)
            {
                if ((car)carriages.Values[i] == car.depot)
                {
                    gotDepot = true;
                    depotIndex = i;
                    //scoreFromDepot = !(i == (carriages.Length - 1));
                }
                i++;
            }

            //Cancel out Royal?
            if (gotDepot && !scoreFromDepot) gotRoyal = false;
            if (gotDepot && scoreFromDepot && (royalIndex < depotIndex)) gotRoyal = false;

            if (scoreFromDepot) startIndex = depotIndex + 1;

            i = startIndex;

            switch ((locoEffect)effect.Value)
            {
                case locoEffect.none:
                    while (i < carriages.Length)
                    {
                        score = score + normalScores[(int)(car)carriages.Values[i]];
                        i++;
                    }

                    i--;

                    if (gotRoyal)
                    {
                        while (i >= startIndex)
                        {
                            int carScore = normalScores[(int)(car)carriages.Values[i]];
                            if ((carScore!=0)&&(royalCars>0))
                            {
                                score = score + carScore;
                                royalCars--;
                            }
                            i--;
                        }
                    }

                    break;

                case locoEffect.exec:
                    while (i < carriages.Length)
                    {
                        score = score + execScores[(int)(car)carriages.Values[i]];
                        i++;
                    }

                    i--;

                    if (gotRoyal)
                    {
                        while (i >= startIndex)
                        {
                            int carScore = execScores[(int)(car)carriages.Values[i]];
                            if ((carScore != 0) && (royalCars > 0))
                            {
                                score = score + carScore;
                                royalCars--;
                            }
                            i--;
                        }
                    }

                    break;

                case locoEffect.freight:
                    while (i < carriages.Length)
                    {
                        score = score + freightScores[(int)(car)carriages.Values[i]];
                        i++;
                    }

                    i--;

                    if (gotRoyal)
                    {
                        while (i >= startIndex)
                        {
                            int carScore = freightScores[(int)(car)carriages.Values[i]];
                            if ((carScore != 0) && (royalCars > 0))
                            {
                                score = score + carScore;
                                royalCars--;
                            }
                            i--;
                        }
                    }

                    break;
            }

            trainScore.Value = score;

            Finish ();
		}
	}
}
