﻿using UnityEngine;
using System;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	[Tooltip("Gets the type of the device!")]
	public class getDeviceType : FsmStateAction
	{
		
		[UIHint(UIHint.FsmEvent)]
		[Tooltip("The event to send if on Android.")]
		public FsmEvent android;

		[UIHint(UIHint.FsmEvent)]
		[Tooltip("The event to send if on iOS.")]
		public FsmEvent apple;

		[UIHint(UIHint.FsmEvent)]
		[Tooltip("The event to send if not sure.")]
		public FsmEvent unknown;

		private string deviceType = "unknown";
		
		public override void Reset()
		{
			android = null;
			apple = null;
			unknown = null;
		}
		
		public override void OnEnter()
		{

			#if UNITY_IPHONE
			deviceType = "apple";
			#endif

			#if UNITY_ANDROID
			deviceType = "android";
			#endif

			switch (deviceType)
			{
			case "apple":
				Fsm.Event(apple);
				break;
			case "android":
				Fsm.Event(android);
				break;
			default:
				Fsm.Event(unknown);
				break;
			}

			Finish ();
		}
	}
}
