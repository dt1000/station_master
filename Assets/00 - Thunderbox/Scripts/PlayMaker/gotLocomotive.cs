﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class gotLocomotive : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(locomotive))]
        public FsmEnum loco;

        [ActionSection("Events")]

        public FsmEvent yes;
        public FsmEvent no;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            loco = null;
            yes = null;
            no = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            if (theData.locomotiveDeck.Contains((locomotive)loco.Value))
            {
                Fsm.Event(yes);
            }
            else
            {
                Fsm.Event(no);
            }

		}
	}
}
