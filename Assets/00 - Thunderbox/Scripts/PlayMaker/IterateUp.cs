// (c) Copyright HutongGames, LLC 2010-2012. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	[Tooltip("Like the default iterate, but only goes up... and will reset when the index is -1.")]
	public class IterateUp : FsmStateAction
	{

		[RequiredField]
		[Tooltip("Start value")]
		public FsmInt startIndex;

		[Tooltip("End value")]
		public FsmInt endIndex;
		
		[Tooltip("increment value at each iteration, absolute value only, it will itself find if it needs to substract or add")]
		public FsmInt increment;
		
		[Tooltip("Event to send to get the next child.")]
		public FsmEvent loopEvent;
		
		[Tooltip("Event to send when we reached the end.")]
		public FsmEvent finishedEvent;
		
		[ActionSection("Result")]
		[Tooltip("The current value of the iteration process")]
		[UIHint(UIHint.Variable)]
		public FsmInt currentIndex;
		
		private bool started = false;
		
		//private bool _up = true;
		public override void Reset()
		{
			startIndex = 0;
			endIndex = 10;
			currentIndex = null;
			loopEvent = null;
			finishedEvent = null;
			increment = 1;
		}
		

		
		public override void OnEnter()
		{
			DoGetNext();

			Finish();
		}
		
		void DoGetNext()
		{

			// reset?
			if (!started || (currentIndex.Value==-1))
			{
				currentIndex.Value = startIndex.Value;
				started = true;
				
				if (loopEvent != null)
				{
					Fsm.Event(loopEvent);
				}
				return;
			}
			
			if (currentIndex.Value >= endIndex.Value)
			{
				started = false;
		
				Fsm.Event(finishedEvent);
			
				return;
			}
			// iterate
			currentIndex.Value =  Mathf.Max(startIndex.Value,currentIndex.Value+Mathf.Abs(increment.Value));
		
			if (loopEvent != null)
			{
				Fsm.Event(loopEvent);
			}
		}
	}
}