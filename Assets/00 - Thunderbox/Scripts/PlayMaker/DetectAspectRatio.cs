﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	[Tooltip("Detects the Aspect Ratio")]
	public class DetectAspectRatio : FsmStateAction
	{
		[ActionSection("Output")]

		public FsmFloat aspect;

		public override void Reset()
		{
            aspect = null;
		}
		
		public override void OnEnter()
		{
            aspect.Value = Camera.main.aspect;
			Finish ();
		}
	}
}
