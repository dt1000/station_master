﻿using UnityEngine;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class calculateTrainScores : FsmStateAction
	{
        [ActionSection("Input")]
        [RequiredField]
        [Tooltip("The Data Manager object.")]
        [CheckForComponent(typeof(StationMasterData))]
        public FsmGameObject dataManager;

        [UIHint(UIHint.Variable)]
        public FsmInt trainScore;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(int))]
        public FsmArray passengerScores;

        public override void Reset()
		{
            dataManager = FsmVariables.GlobalVariables.GetFsmGameObject("UX Manager");
            trainScore = null;
            passengerScores = null;
        }
		
		public override void OnEnter()
		{            
            StationMasterData theData = dataManager.Value.GetComponent<StationMasterData>();

            //Clear the last set of scores
            theData.trainScores.RemoveRange(0, theData.trainScores.Count);

            int i = 0;
            while (i<theData.players.Count)
            {
                theData.trainScores.Add(new trainScore(theData.players[i].piece, theData.players[i].name, (trainScore.Value * (int)passengerScores.Values[i])));
                i++;
            }

            theData.trainScores.Sort((trainScoreA, trainScoreB) => trainScoreB.score.CompareTo(trainScoreA.score));

            Finish ();
		}
	}
}
