﻿using UnityEngine;
using UnityEngine.UI.Extensions;
using StationMaster;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("Thunderbox")]
	public class getScrollerScores : FsmStateAction
	{

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(GameObject))]
        public FsmArray playerUIObjects;

        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(int))]
        public FsmArray passengerScores;

        public override void Reset()
		{
            playerUIObjects = null;
            passengerScores = null;
        }
		
		public override void OnEnter()
		{
            passengerScores.Resize(0);

            int i = 0;
            while ( i < playerUIObjects.Length)
            {
                VerticalScrollSnap theScroller = ((GameObject)playerUIObjects.Values[i]).GetComponent<VerticalScrollSnap>();
                int playerScore = theScroller.CurrentPage;
                playerScore--;
                passengerScores.Resize(passengerScores.Length + 1);
                passengerScores.Values[passengerScores.Length - 1] = playerScore;
                i++;
            }
            Finish ();
		}
	}
}
