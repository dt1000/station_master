﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StationMaster
{

    public enum playerPiece { briefcase, hat, perfume, boot, book, teacup, camera, bowler, hipflask, handbag, ticket, teapot };

    public enum tokenSet { normal, deluxe, both };

    public enum locomotive
    {
        loco_3,
        loco_3_exec,
        loco_4,
        loco_4_exec,
        loco_5_a,
        loco_5_b,
        loco_5_freight,
        loco_6,
        loco_7,
        loco_8,
        depot
    };

    public enum car
    {
        plus_1,
        plus_2,
        plus_3,
        plus_4,
        plus_5,
        plus_6,
        minus_1,
        minus_2,
        minus_3,
        minus_4,
        minus_5,
        minus_6,
        exec_6_3,
        exec_7_4,
        royal,
        depot,
        caboose
    };


    public enum locoEffect
    {
        none,
        exec,
        freight
    };
    public class player
    {
        public playerPiece piece;
        public string name;
        public int score;
        public int topTrain;

        public player (playerPiece p1, string p2)
        {
            piece = p1;
            name = p2;
            score = 0;
            topTrain = 0;
        }
    }

    public class train
    {
        public locomotive locoType;
        public List<car> cars;

        public train(locomotive p1, List<car> p2)
        {
            locoType = p1;
            cars = p2;
        }
    }

    public class trainScore
    {
        public playerPiece piece;
        public string playerName;
        public int score;

        public trainScore(playerPiece p1, string p2, int p3)
        {
            piece = p1;
            playerName = p2;
            score = p3;
        }
    }
}
